from django.http import HttpRequest, HttpResponse
from django.views.generic import View
from django.shortcuts import render
# Create your views here.

def index(request):
    if request.method =='GET':
        params = {}
        params["name"] = "Django is not mine"
        return render(request, 'base.html', params)
    if request.method =='POST':
        return HttpResponse('I am called from post Request')
